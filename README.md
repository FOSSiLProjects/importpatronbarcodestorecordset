# Import Patron Barcodes to Record Set

### Requires:

* AutoHotkey
* Windows (7, 8, 10)
* Polaris ILS (Supported: 4.1R2 - 5.2. Should work with newer versions, but I've not tested it.)

---

This is a script written in AutoHotkey that uses a CSV file to import a set of patron barcodes into a Patron Record Set in Polaris ILS. This is especially useful when gathering patron barcodes from a SQL query. The barcodes can be pasted to a CSV file and then you can use this script to push that information into a Patron Record Set.

Average import time is ~1 second per barcode.

### To Use

This script is set to work from the C:\Temp directory, but this can be adjusted as you desire. A CSV of patron barcodes obtained through SQL provides the input for the script. To set up, open up a Patron Record Set, and then set to add patrons by scanning. Once the box is open to accept barcodes, launch the script.

I recommend standing by while the script runs. The timing works to allow for lag and database slowness, but I always keep an eye on it, just in case.

### Questions?

cyberpunklibrarian (at) protonmail (dot) com